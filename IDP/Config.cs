﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace IDP
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("api")
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new ApiResource[]
            {
                new ApiResource("blogAuthenticationApi", "Blog Authentication Api")
                {
                    Scopes = { "api" }
                }
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "blog.client.resource",
                    ClientName = "Blog Client Credential",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    ClientSecrets = { new Secret("y_K+j.[F9zYa6#E4".Sha256()) },
                    RequireClientSecret = false,
                    AllowedScopes = { "api" },
                    AccessTokenLifetime = 18000
                },
                new Client
                {
                    ClientId = "blog.client.code",
                    ClientName = "Blog Client Authorization Code",
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris =
                    {
                      "http://localhost:4200/sign-in-callback"
                    },
                    PostLogoutRedirectUris =
                    {
                        "http://localhost:4200/sign-out-callback"
                    },
                    ClientSecrets = { new Secret("U^un8mbUh_z*bxEb".Sha256()) },
                    AllowedScopes = { 
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api" 
                    },
                    RequireClientSecret = false,
                    RequirePkce = true,
                    AlwaysIncludeUserClaimsInIdToken = false,
                    AllowedCorsOrigins =
                    {
                        "http://localhost:4200"
                    }
                },
                new Client
                {
                    ClientId = "client.code.test",
                    ClientName = "Client Authorization Code Test",
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris =
                    {
                      "http://localhost:4200"
                    },
                    PostLogoutRedirectUris =
                    {
                        "http://localhost:4200"
                    },
                    ClientSecrets = { new Secret("U^un8mbUh_z*bxEb".Sha256()) },
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api"
                    },
                    RequirePkce = true,
                    RequireClientSecret = false,
                    AllowedCorsOrigins =
                    {
                        "http://localhost:4200"
                    },
                    AccessTokenLifetime = 3600
                },
            };
    }
}