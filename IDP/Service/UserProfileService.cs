﻿using IdentityModel;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Infrastructure.Entity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IDP.Service
{
    public class UserProfileService : IProfileService
    {
        private readonly UserManager<User> _userManager;
        private readonly IUserClaimsPrincipalFactory<User> _claimsFactory;
        public UserProfileService(UserManager<User> userManager, IUserClaimsPrincipalFactory<User> claimsFactory)
        {
            this._userManager = userManager;
            this._claimsFactory = claimsFactory;
        }
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var subjectId = context.Subject.GetSubjectId();
            var user = await this._userManager.FindByIdAsync(subjectId);
            var principal = await _claimsFactory.CreateAsync(user);

            var claims = principal.Claims.ToList();
            claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();
            claims.Add(new Claim(JwtClaimTypes.Name, user.LastName));
            claims.Add(new Claim(JwtClaimTypes.Email, user.Email));
            //claims.Add(new Claim(JwtClaimTypes.FamilyName, user.FirstName + " " + user.LastName));
            //claims.Add(new Claim(JwtClaimTypes.MiddleName, user.LastName));
            //claims.Add(new Claim(JwtClaimTypes.NickName, user.LastName));
            //claims.Add(new Claim(JwtClaimTypes.PreferredUserName, user.UserName));
            //claims.Add(new Claim(JwtClaimTypes.Picture, string.Empty));
            //claims.Add(new Claim(JwtClaimTypes.WebSite, string.Empty));
            //claims.Add(new Claim(JwtClaimTypes.Gender, string.Empty));
            //claims.Add(new Claim(JwtClaimTypes.BirthDate, string.Empty));
            //claims.Add(new Claim(JwtClaimTypes.ZoneInfo, string.Empty));
            //claims.Add(new Claim(JwtClaimTypes.Locale, string.Empty));
            //claims.Add(new Claim(JwtClaimTypes.UpdatedAt, DateTime.Now.ToUniversalTime().ToString()));

            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }
}
